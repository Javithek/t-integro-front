import { Injectable, SkipSelf, Optional } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { Acompanante } from "./acompanante";
import { Paciente } from "./paciente";

@Injectable()

export class AppService{
    
    private msg ?: string;
    private pacienteEditar ?: Paciente;
    private acompananteEditar ?: Acompanante;

    constructor(@Optional() @SkipSelf() sharedService?: AppService){
        if(sharedService){
            throw new Error('App Service is already loaded');
        }
        console.info('AppService Created');
    }

    getAcompananteEditar(){
        return this.acompananteEditar;
    }

    setAcompananteEditar(acompanante : Acompanante){
        this.acompananteEditar = acompanante;
    }

    getPacienteEditar() {
        return this.pacienteEditar;
    }

    setPacienteEditar(paciente : Paciente){
        this.pacienteEditar = paciente;
    }

    getMsg() {
        return this.msg;
    }

    setMsg(newMsg:string){
        this.msg = newMsg;
    }
}