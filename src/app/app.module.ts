import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { Home } from './home';
import { AppComponent } from './app.component';
import { CardRouter } from './card';
import { AppRoutingModule } from './app-routing.module';
import { MatInputModule } from '@angular/material/input';
import { CreatePaciente } from './create';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FindPaciente } from './find';
import { AppService } from './app.service';

@NgModule({
  declarations: [
    AppComponent,
    CardRouter,
    CreatePaciente,
    FindPaciente,
    Home
  ],
  imports: [
    BrowserModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    AppRoutingModule
  ],
  providers: [ AppService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
