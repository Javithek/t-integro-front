export class Paciente {
    idPaciente?: String;
    apellidoPaterno?: String;
    apellidoMaterno?: String;
    name?: String;
    tipoDocumento?: String;
    numeroDocumento?: String;
    fechaNacimiento?: String;
    lugarNacimiento?: String;
    direccion?: String;
    estado?: Number;
    edit?: String;
    delete?: String;
}