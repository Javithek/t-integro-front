import {Component, Input, EventEmitter, Output, OnInit, ViewChild, Injectable} from '@angular/core';
import { Paciente } from './paciente';
import axios, { AxiosResponse, AxiosRequestConfig, RawAxiosRequestHeaders } from 'axios';
import { CreatePaciente } from './create';
import { AppService } from './app.service';
import { Router } from '@angular/router';
import { Acompanante } from './acompanante';



@Component({
    selector: 'find',
    templateUrl: './find.html',
    styleUrls: ['./find.css']
})

@Injectable()
export class FindPaciente implements OnInit {
 
    pacientes?: Paciente[];
    baseUrl = "http://localhost:8282/app";
    static element?: HTMLInputElement;
    result = new Array();
    constructor (private appSrvInj : AppService, private router: Router) {
    

    // this.veryEdit();
      //  this.appSrvInj.setMsg('NEW MESSAGE FROM FIND aaaa');
     //   this.appSrv.setIdPacienteEdit(this.idPaci);
    }
 
    ngOnInit() {
        //this.initPacientes();
        this.loadTypeDocument();
        this.appSrvInj.setMsg('NEW MESSAGE FROM FIND aaaa');
    }

    registrarPaciente(event:any) {
        this.router.navigateByUrl('/create');
    }

    initPacientes() : void {
        this.pacientes = [
            {name:'Javier', tipoDocumento:'CE', numeroDocumento:'00331111', fechaNacimiento:'09/11/1995', estado:1, edit:'https://ipfs.io/ipfs/Qmd2u89pWP87xVXFiXcuZGqx3PLnr8bzCC2m2LyL46awcq', delete:'https://ipfs.io/ipfs/QmUytwAgdBdqcZi4ATk4BdkY6SDLZVJq7SguwcTjdSHywP'}, 
            {name:'Keiner', tipoDocumento:'CE', numeroDocumento:'00332222', fechaNacimiento:'09/11/1995', estado:0, edit:'https://ipfs.io/ipfs/Qmd2u89pWP87xVXFiXcuZGqx3PLnr8bzCC2m2LyL46awcq', delete:'https://ipfs.io/ipfs/QmUytwAgdBdqcZi4ATk4BdkY6SDLZVJq7SguwcTjdSHywP'}
        ];
    }

    urlEdit = 'https://ipfs.io/ipfs/Qmd2u89pWP87xVXFiXcuZGqx3PLnr8bzCC2m2LyL46awcq';
    urlSave = 'https://ipfs.io/ipfs/QmTxTVtdHxfDNVmSi79wRKXgWeDnUZUNeqUHHexmemghAG';


    listAcompanantes = new Array();

    async loadAcompanantes() {
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/acompanante/list', config);
        for(let i=0; i<searchResponse.data.length; i++){
            var obj = new Acompanante();
            obj.idAcompanante = searchResponse.data[i].idPacienteAcompanante;
            obj.idPaciente = searchResponse.data[i].idPaciente;
            obj.tipoDocumento = searchResponse.data[i].idTipoDocIde;
            obj.numeroDocumento = searchResponse.data[i].nroDocIde;
            obj.name = searchResponse.data[i].nombres;
            obj.apellidoPaterno = searchResponse.data[i].apellidoPaterno;
            obj.apellidoMaterno = searchResponse.data[i].apellidoMaterno;
            obj.direccion = searchResponse.data[i].direccion;
            obj.numeroContacto = searchResponse.data[i].nroTelefonoContacto;
            obj.fechaNacimiento = searchResponse.data[i].fechaNacimiento;
            obj.parentesco = searchResponse.data[i].idParentesco;
            this.listAcompanantes.push(obj);
        }
        console.log(this.listAcompanantes);
    }


    typeDocumentsKey = new Map<String, String>();

    async loadPacientes(idDoc:any, names:any, typeDoc:any) : Promise<Array<Paciente>>{
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        idDoc = String(idDoc);
        const searchResponse : AxiosResponse = await client.get('/paciente/list', config);
        this.result = new Array();
        console.log(searchResponse.data);
        console.log(names);
        for(let i=0; i<searchResponse.data.length; i++){
            if(idDoc != '' && idDoc != null && idDoc != undefined){
                if(idDoc == searchResponse.data[i].nroDocIde && typeDoc == this.typeDocuments.get(searchResponse.data[i].idTipoDocIde)){
                    var obj = new Paciente();
                    obj.idPaciente = searchResponse.data[i].idPaciente;
                    obj.apellidoPaterno = searchResponse.data[i].apellidoPaterno;
                    obj.apellidoMaterno = searchResponse.data[i].apellidoMaterno;
                    obj.name = searchResponse.data[i].nombres;
                    obj.tipoDocumento = this.typeDocuments.get(searchResponse.data[i].idTipoDocIde);
                    this.typeDocumentsKey.set(String(this.typeDocuments.get(searchResponse.data[i].idTipoDocIde)), searchResponse.data[i].idTipoDocIde);
                    obj.numeroDocumento = searchResponse.data[i].nroDocIde;
                    obj.fechaNacimiento = searchResponse.data[i].fechaNacimiento;
                    obj.lugarNacimiento = searchResponse.data[i].lugarNacimiento;
                    obj.direccion = searchResponse.data[i].direccion;
                    obj.estado = 1;
                    obj.edit = 'https://ipfs.io/ipfs/Qmd2u89pWP87xVXFiXcuZGqx3PLnr8bzCC2m2LyL46awcq';
                    obj.delete = 'https://ipfs.io/ipfs/QmUytwAgdBdqcZi4ATk4BdkY6SDLZVJq7SguwcTjdSHywP';
                    this.result.push(obj);
                }
            }else if(names != '' && names != null && names != undefined){
                console.log(searchResponse.data[i].apellidoPaterno.replace(' ', '') + searchResponse.data[i].apellidoMaterno.replace(' ', '') + searchResponse.data[i].nombres.replace(' ', ''));
                if(names == String(searchResponse.data[i].apellidoPaterno.replace(' ', '') + searchResponse.data[i].apellidoMaterno.replace(' ', '') + searchResponse.data[i].nombres.replace(' ', ''))){
                    var obj = new Paciente();
                    obj.apellidoPaterno = searchResponse.data[i].apellidoPaterno;
                    obj.apellidoMaterno = searchResponse.data[i].apellidoMaterno;
                    obj.name = searchResponse.data[i].nombres;
                    obj.tipoDocumento = this.typeDocuments.get(searchResponse.data[i].idTipoDocIde);
                    obj.numeroDocumento = searchResponse.data[i].nroDocIde;
                    obj.fechaNacimiento = searchResponse.data[i].fechaNacimiento;
                    obj.lugarNacimiento = searchResponse.data[i].lugarNacimiento;
                    obj.direccion = searchResponse.data[i].direccion;
                    obj.estado = 1;
                    obj.edit = 'https://ipfs.io/ipfs/Qmd2u89pWP87xVXFiXcuZGqx3PLnr8bzCC2m2LyL46awcq';
                    obj.delete = 'https://ipfs.io/ipfs/QmUytwAgdBdqcZi4ATk4BdkY6SDLZVJq7SguwcTjdSHywP';
                    this.result.push(obj);
                }
            }
            
        }
        console.log(this.result);
        return this.result;
    }

    typeDocumentos = [{descripcion:String, value:String}];
    typeDocuments = new Map<String, String>();

    async loadTypeDocument(){
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/identity/tipo-documento', config);
        for(let i=0; i<searchResponse.data.length; i++){
            this.typeDocumentos[i] = {
                descripcion:searchResponse.data[i].codigoIEDS,
                value:searchResponse.data[i].idTipoDocumentoIdentidad
            };
            this.typeDocuments.set(searchResponse.data[i].idTipoDocumentoIdentidad, searchResponse.data[i].codigoIEDS);
        }
        console.log(this.typeDocumentos);
    }

    editEvent(event:any, id:any, idDoc:any) : void {

        var actionBtn = (document.getElementById('edit-btn-' + idDoc) as any);
        var name = (document.getElementById('input-find-name-' + id) as any);

        if(actionBtn != null) {
            if(actionBtn.src == this.urlEdit){
                //var create = new CreatePaciente();
                var pacienteEdit = new Paciente();
               // pacienteEdit.numeroDocumento = (document.getElementById('input-find-numero-documento') as any).value;
                pacienteEdit.idPaciente = this.result[0].idPaciente;
                pacienteEdit.numeroDocumento = this.result[0].numeroDocumento;
                pacienteEdit.tipoDocumento = this.result[0].tipoDocumento;
                pacienteEdit.apellidoPaterno = this.result[0].apellidoPaterno;
                pacienteEdit.apellidoMaterno = this.result[0].apellidoMaterno;
                pacienteEdit.name = this.result[0].name;
                pacienteEdit.tipoDocumento = this.typeDocumentsKey.get(this.result[0].tipoDocumento);
                pacienteEdit.fechaNacimiento = this.result[0].fechaNacimiento;
                pacienteEdit.lugarNacimiento = this.result[0].lugarNacimiento;
                pacienteEdit.direccion = this.result[0].direccion;
                console.log(pacienteEdit);
                this.appSrvInj.setPacienteEditar(pacienteEdit);
                var acompananteEdit = new Acompanante();
                this.listAcompanantes.forEach((value, index) => {
                    if(pacienteEdit.idPaciente == value.idPaciente){
                        acompananteEdit = value;
                        return;
                    }
                });
                this.appSrvInj.setAcompananteEditar(acompananteEdit);
                actionBtn.src = this.urlSave;
                name.disabled = false;
                this.router.navigateByUrl('/create');
            }else{
                //guardar en bd y cambiar icono
                actionBtn.src = this.urlEdit;
                name.disabled = true;
            }
        }
    }

    deleteEvent(event:any, idDoc:any) : void{
        alert('Paciente a eliminar ' + idDoc);
    }

    async findPaciente(event:any) {
        this.listAcompanantes = new Array();
        var typeDoc = (document.getElementById('select-tipo-doc-find') as any);
        var valueTypeDoc = typeDoc.options[typeDoc.selectedIndex].text;
        var inputDoc = (document.getElementById('input-nro-doc-find') as any);
        var inputLastName = (document.getElementById('input-apellidos-find') as any);
        var inputName = (document.getElementById('input-nombres-find') as any);
        if(valueTypeDoc != null && valueTypeDoc != '' 
        && valueTypeDoc != undefined && valueTypeDoc != '...' && inputDoc.value != null && inputDoc.value != ''
        && inputDoc.value != undefined){
            //busqueda por idDoc http
            //this.initPacientes();
            this.pacientes = await this.loadPacientes(inputDoc.value, undefined, valueTypeDoc);
            if(this.pacientes.length > 0){
                await this.loadAcompanantes();
            }
        }else if(inputLastName.value != null && inputLastName.value != '' && inputLastName.value != undefined
        && inputName.value != null && inputName.value != '' && inputName.value != undefined){
            //busqueda por names http
            this.pacientes = await this.loadPacientes(undefined, String(inputLastName.value.replace(' ', '') + inputName.value.replace(' ', '')), valueTypeDoc);
        }else{
            this.pacientes = [];
        }
    }
}