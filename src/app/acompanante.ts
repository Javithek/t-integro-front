export class Acompanante{
    idPaciente?: String;
    idAcompanante?: String;
    apellidoPaterno?: String;
    apellidoMaterno?: String;
    name?: String;
    tipoDocumento?: String;
    numeroDocumento?: String;
    fechaNacimiento?: String;
    numeroContacto?: String;
    parentesco?: String;
    direccion?: String;
}