import {Component, Injectable, Input, OnInit, ViewChild, AfterViewInit} from '@angular/core';
import axios, { AxiosResponse, AxiosRequestConfig, RawAxiosRequestHeaders } from 'axios';
import { FindPaciente } from './find';
import { AppService } from './app.service';
import { Paciente } from './paciente';
import { Router } from '@angular/router';
import { Acompanante } from './acompanante';

@Component({
    selector: 'create',
    templateUrl: './create.html',
    styleUrls: ['./create.css']
})

@Injectable()

export class CreatePaciente implements OnInit{

    baseUrl = "http://localhost:8282/app";

    @Input() msg?: string;
    @Input() pacienteEdit ?: Paciente;
    @Input() acompananteEdit ?: Acompanante;

    constructor (private appSrv : AppService, private router : Router) {
       /// this.findPaci = find;
        //this.findPaci.appSrv?.getMsg.subscribe(m => this.msg = m);
    }

    typeDocumentos = [{descripcion:String, value:String}];
    typeSexo = [{descripcion:String, value:String}];
    typeUbigeo = [{departamento:String, provincia:String, distrito:String, valueDepartamento:String, valueProvincia:String, valueDistrito:String}]
    typeParentesco = [{descripcion:String, value:String}];

    ngOnInit(): void {
        this.loadTypeDocument();
        this.loadTypeSexo();
        this.loadUbigeo();
        this.loadParentesco();
        this.verifyEdit();
    }

    backHome(event:any){
        this.router.navigateByUrl('/');
    }

    verifyEdit() : void {
        this.pacienteEdit = this.appSrv.getPacienteEditar();
        this.acompananteEdit = this.appSrv.getAcompananteEditar();
        if(this.pacienteEdit?.numeroDocumento != '' && this.pacienteEdit?.numeroDocumento != null && this.pacienteEdit.numeroDocumento != undefined){
           // alert('Paciente a Editar: ' + this.pacienteEdit.name);
            (document.getElementById('input-nro-doc-paciente') as any).value = this.pacienteEdit.numeroDocumento;
            (document.getElementById('input-apellido-paterno-paciente') as any).value = this.pacienteEdit.apellidoPaterno;
            (document.getElementById('input-apellido-materno-paciente') as any).value = this.pacienteEdit.apellidoMaterno;
            (document.getElementById('input-nombres-paciente') as any).value = this.pacienteEdit.name;
            (document.getElementById('input-fecha-nacimiento-paciente') as any).value = this.pacienteEdit.fechaNacimiento;
            this.calcularEdad(event, 'input-edad-paciente', 'input-fecha-nacimiento-paciente');
            (document.getElementById('input-lugar-nacimiento-paciente') as any).value = this.pacienteEdit.lugarNacimiento;
            (document.getElementById('input-direccion-paciente') as any).value = this.pacienteEdit.direccion;
            (document.getElementById('btn-registro-actualizar') as any).innerText = 'Actualizar';
        }
        if(this.acompananteEdit?.numeroDocumento != '' && this.acompananteEdit?.numeroDocumento != null && this.acompananteEdit.numeroDocumento != undefined){
            (document.getElementById('input-nro-doc-acompanante') as any).value = this.acompananteEdit.numeroDocumento;
            (document.getElementById('input-apellido-paterno-acompanante') as any).value = this.acompananteEdit.apellidoPaterno;
            (document.getElementById('input-apellido-materno-acompanante') as any).value = this.acompananteEdit.apellidoMaterno;
            (document.getElementById('input-nombres-acompanante') as any).value = this.acompananteEdit.name;
            (document.getElementById('input-fecha-nacimiento-acompanante') as any).value = this.acompananteEdit.fechaNacimiento;
            this.calcularEdad(event, 'input-edad-acompanante', 'input-fecha-nacimiento-acompanante');
            (document.getElementById('input-direccion-acompanante') as any).value = this.acompananteEdit.direccion;
            (document.getElementById('input-telefono-contracto-acompanante') as any).value = this.acompananteEdit.direccion;
        }
    }

    async loadParentesco(){
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/relation/parentesco', config);
        for(let i=0; i<searchResponse.data.length; i++){
            this.typeParentesco[i] = {
                descripcion: searchResponse.data[i].descripcionParentesco,
                value: searchResponse.data[i].idParentesco
            };
        }
        console.log(this.typeParentesco);
    }

    listUbigeoResponse = new Array();
    listDepartamento = new Array();
    listProvincia = new Array();
    listDistrito = new Array();

    listDepartamentoAcompanante = new Array();
    listProvinciaAcompanante = new Array();
    listDistritoAcompanante = new Array();

    changeProvincia(event:any, id:any, identity:any) : void{
        var idElement = String(id);
        var select = (document.getElementById(idElement) as any);
        var descSelectProvincia = select.options[select.selectedIndex].text;
        var valueSelectProvincia = select.options[select.selectedIndex].value;
        let hashDistrito = new Map<String, {value:String, desc:String}>();

        identity = String(identity);
        if(identity == 'select-provincia-paciente'){
            this.listDistrito = new Array();
        }else if(identity == 'select-provincia-acompanante'){
            this.listDistritoAcompanante = new Array();
        }
        
        //alert('Cod: ' + valueSelectProvincia + ' Desc: ' + descSelectProvincia);
        for(let i=0; i<this.listUbigeoResponse.length; i++){
            if(this.listUbigeoResponse[i].codigoProvincia == valueSelectProvincia){
                if(this.listUbigeoResponse[i].descripcionProvincia != ''){
                    hashDistrito.set(this.listUbigeoResponse[i].codigoDistrito, {
                        value: this.listUbigeoResponse[i].codigoDistrito,
                        desc: this.listUbigeoResponse[i].descripcionDistrito
                    });
                }
            }
        }

        if(identity == 'select-provincia-paciente'){
            hashDistrito.forEach((value, key) => {
                this.listDistrito.push(value);
            });
        }else if(identity == 'select-provincia-acompanante'){
            hashDistrito.forEach((value, key) => {
                this.listDistritoAcompanante.push(value);
            });
        }
        console.log(this.listDistrito);
    }


    changeDepartamento(event:any, id:any, identity:any) : void{
        var idElement = String(id);
        var select = (document.getElementById(idElement) as any);
        var descSelectDepartamento = select.options[select.selectedIndex].text;
        var valueSelectDepartamento = select.options[select.selectedIndex].value;
        let hashProvincia = new Map<String, {value:String, desc:String}>();
        identity = String(identity);
        if(identity == 'select-departamento-paciente'){
            this.listProvincia = new Array();
            this.listDistrito = new Array();
        }else if(identity == 'select-departamento-acompanante'){
            this.listProvinciaAcompanante = new Array();
            this.listDistritoAcompanante = new Array();
        }
        
        //alert('Cod: ' + valueSelectDepartamento + ' Desc: ' + descSelectDepartamento);
        for(let i=0; i<this.listUbigeoResponse.length; i++){
            if(this.listUbigeoResponse[i].codigoDepartamento == valueSelectDepartamento){
                if(this.listUbigeoResponse[i].descripcionProvincia != ''){
                    hashProvincia.set(this.listUbigeoResponse[i].codigoProvincia, {
                        value: this.listUbigeoResponse[i].codigoProvincia,
                        desc: this.listUbigeoResponse[i].descripcionProvincia
                    });
                }
            }
        }

        if(identity == 'select-departamento-paciente'){
            hashProvincia.forEach((value, key) => {
                this.listProvincia.push(value);
            });
        }else if(identity == 'select-departamento-acompanante'){
            hashProvincia.forEach((value, key) => {
                this.listProvinciaAcompanante.push(value);
            });
        }
    }

    async loadUbigeo(){
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/address/ubigeo', config);
        let hashDepartamento = new Map<String, {value:String, desc:String}>();
        let hashProvincia = new Map<String, {value:String, desc:String}>();
        let listProvincia = new Array();

        this.listUbigeoResponse = searchResponse.data;

        for(let i=0; i<searchResponse.data.length; i++){
            hashDepartamento.set(searchResponse.data[i].codigoDepartamento, {
                value: searchResponse.data[i].codigoDepartamento,
                desc: searchResponse.data[i].descripcionDepartamento
            });
            
            this.typeUbigeo[i] = {
                departamento: searchResponse.data[i].descripcionDepartamento,
                provincia: searchResponse.data[i].descripcionProvincia,
                distrito: searchResponse.data[i].descripcionDistrito,
                valueDepartamento: searchResponse.data[i].codigoDepartamento,
                valueProvincia: searchResponse.data[i].codigoProvincia,
                valueDistrito: searchResponse.data[i].codigoDistrito
            }
        }

        hashDepartamento.forEach((value, key) => {
            this.listDepartamento.push(value);
        });

    
        console.log(this.listDepartamento);
        console.log(this.typeUbigeo);
    }

    async loadTypeSexo(){
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/person/sexo', config);
        for(let i=0; i<searchResponse.data.length; i++){
            this.typeSexo[i] = {
                descripcion:searchResponse.data[i].descripcionSexo,
                value:searchResponse.data[i].idSexo
            }
        }
        console.log(this.typeSexo);
    }

    async createAcompanante(dataAcompanante:{}) : Promise<String> {
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponseAcompanante : AxiosResponse = await client.post('/acompanante/crear', dataAcompanante, config);
        var idAcompanante = searchResponseAcompanante.data.idPacienteAcompanante
        return idAcompanante;
    }

    async actualizarAcompanante(dataAcompanante:{}, idAcompanante:Number) : Promise<String>{
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponseAcompanante : AxiosResponse = await client.put('/acompanante/actualizar?id=' + idAcompanante, dataAcompanante, config);
        var idAcompananteResponse = searchResponseAcompanante.data.idPacienteAcompanante;
        return idAcompananteResponse;
    }

    async actualizarPaciente(dataPaciente:{}, idPaciente:Number) : Promise<String> {
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponsePaciente : AxiosResponse = await client.put('/paciente/actualizar?id=' + idPaciente, dataPaciente, config);
        var idPacienteResponse = searchResponsePaciente.data.idPaciente;
        return idPacienteResponse;
    }

    async createPaciente(dataPaciente:{}) : Promise<String> {
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponsePaciente : AxiosResponse = await client.post('/paciente/crear', dataPaciente, config);
        var idPaciente = searchResponsePaciente.data.idPaciente;
        return idPaciente;
    }

    async loadTypeDocument(){
        const client = axios.create({
            baseURL: this.baseUrl,
        });
        const config : AxiosRequestConfig = {
            headers: {
                "Access-Control-Allow-Origin" : "*",
                'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS'
            }
        }
        const searchResponse : AxiosResponse = await client.get('/identity/tipo-documento', config);
        for(let i=0; i<searchResponse.data.length; i++){
            this.typeDocumentos[i] = {
                descripcion:searchResponse.data[i].codigoIEDS,
                value:searchResponse.data[i].idTipoDocumentoIdentidad
            };
        }
        console.log(this.typeDocumentos);
    }


    calcularEdad(event:any, input:any, inputFecha:any) : void {
        //alert((document.getElementById('input-fecha-nacimiento-paciente') as any).value);
        var today = new Date();
        var yearToday = today.getUTCFullYear();
        var mesToday = today.getUTCMonth() + 1;
        var dayToday = today.getUTCDate();
        var idFecha = String(inputFecha);
        var fechaIngresada = (document.getElementById(idFecha) as any).value;
        var formatDate = new Date(fechaIngresada);
        var yearIngresado = formatDate.getUTCFullYear();
        var mesIngresado = formatDate.getUTCMonth() + 1;
        var dayIngresado = formatDate.getUTCDate();

        var ageAprox = (yearToday - yearIngresado) - 1;

        if(mesToday == mesIngresado){
            if(dayToday == dayIngresado){
                ageAprox++;
            }else if(dayIngresado < dayToday){
                ageAprox++;
            }
        }else if(mesIngresado < mesToday){
            ageAprox++;
        }
        
        var id = String(input);

        (document.getElementById(id) as any).value = ageAprox;
    }   


    async savePaciente(event:any){

        // datos paciente

        var typeDocPaciente = (document.getElementById('select-tipo-doc-create-paciente') as any);
        var valueTypeDocPaciente = typeDocPaciente.options[typeDocPaciente.selectedIndex].text;
        var valueTwoTypeDocPaciente = typeDocPaciente.options[typeDocPaciente.selectedIndex].value;
        var inputNroDocPaciente = (document.getElementById('input-nro-doc-paciente') as any);
        var inputApellidoPaternoPaciente = (document.getElementById('input-apellido-paterno-paciente') as any);
        var inputApellidoMaternoPaciente = (document.getElementById('input-apellido-materno-paciente') as any);
        var inputNamesPaciente = (document.getElementById('input-nombres-paciente') as any);
        var typeSexoPaciente = (document.getElementById('select-tipo-sexo-paciente') as any);
        var valueSexoPaciente = typeSexoPaciente.options[typeSexoPaciente.selectedIndex].text;
        var valueTwoSexoPaciente = typeSexoPaciente.options[typeSexoPaciente.selectedIndex].value;
        var inputFechaNacimiento = (document.getElementById('input-fecha-nacimiento-paciente') as any);
        var inputEdadPaciente = (document.getElementById('input-edad-paciente') as any);
        var inputLugarNacimiento = (document.getElementById('input-lugar-nacimiento-paciente') as any);
        var direccionPaciente = (document.getElementById('input-direccion-paciente') as any);
        var typeDeparPaciente = (document.getElementById('select-departamento-paciente') as any);
        var valueDeparPaciente = typeDeparPaciente.options[typeDeparPaciente.selectedIndex].text;
        var valueTwoDeparPaciente = typeDeparPaciente.options[typeDeparPaciente.selectedIndex].value;
        var typeProvinciaPaciente = (document.getElementById('select-provincia-paciente') as any);
        var valueProvinciaPaciente = typeProvinciaPaciente.options[typeProvinciaPaciente.selectedIndex].text;
        var valueTwoProvinciaPaciente = typeProvinciaPaciente.options[typeProvinciaPaciente.selectedIndex].value;
        var typeDistritoPaciente = (document.getElementById('select-distrito-paciente') as any);
        var valueDistritoPaciente = typeDistritoPaciente.options[typeDistritoPaciente.selectedIndex].text;
        var valueTwoDistritoPaciente = typeDistritoPaciente.options[typeDistritoPaciente.selectedIndex].value;

        // datos acompa;ante

        var typeDocAcompanante = (document.getElementById('select-tipo-doc-acompanante') as any);
        var valueDocAcompanante = typeDocAcompanante.options[typeDocAcompanante.selectedIndex].text;
        var valueTwoDocAcompanante = typeDocAcompanante.options[typeDocAcompanante.selectedIndex].value;
        var inputNroDocAcompanante = (document.getElementById('input-nro-doc-acompanante') as any);
        var inputApellidoPaternoAcompanante = (document.getElementById('input-apellido-paterno-acompanante') as any);
        var inputApellidoMaternoAcompanante = (document.getElementById('input-apellido-materno-acompanante') as any);
        var inputNamesAcompanante = (document.getElementById('input-nombres-acompanante') as any);
        var inputFechaNacimientoAcompanante = (document.getElementById('input-fecha-nacimiento-acompanante') as any);
        var inputEdadAcompanante = (document.getElementById('input-edad-acompanante') as any);
        var typeParentescoAcompanante = (document.getElementById('select-tipo-doc-parentesco-acompanante') as any);
        var valueParentescoAcompanante = typeParentescoAcompanante.options[typeParentescoAcompanante.selectedIndex].text;
        var valueTwoParentescoAcompanante = typeParentescoAcompanante.options[typeParentescoAcompanante.selectedIndex].value;
        var inputTelefonoContactoAcompanante = (document.getElementById('input-telefono-contracto-acompanante') as any);
        var inputDireccionAcompanante = (document.getElementById('input-direccion-acompanante') as any);
        var typeDeparAcompanante = (document.getElementById('select-departamento-acompanante') as any);
        var valueDeparAcompanante = typeDeparAcompanante.options[typeDeparAcompanante.selectedIndex].text;
        var valueTwoDeparAcompanante = typeDeparAcompanante.options[typeDeparAcompanante.selectedIndex].value;
        var typeProvinciaAcompanante = (document.getElementById('select-provincia-acompanante') as any);
        var valueProvinciaAcompanante = typeProvinciaAcompanante.options[typeProvinciaAcompanante.selectedIndex].text;
        var valueTwoProvinciaAcompanante = typeProvinciaAcompanante.options[typeProvinciaAcompanante.selectedIndex].value;
        var typeDistritoAcompanante = (document.getElementById('select-distrito-acompanante') as any);
        var valueDistritoAcompanante = typeDistritoAcompanante.options[typeDistritoAcompanante.selectedIndex].text;
        var valueTwoDistritoAcompanante = typeDistritoAcompanante.options[typeDistritoAcompanante.selectedIndex].value;
        
        //alert(valueTwoDeparPaciente + "-" + valueTwoProvinciaPaciente + "-" + valueTwoDistritoPaciente);
        var idPacienteResponse = Number();
        if(valueTypeDocPaciente != null && valueTypeDocPaciente != '' && valueTypeDocPaciente != '...' && valueTypeDocPaciente != undefined){
            if(inputNroDocPaciente.value != null && inputNroDocPaciente.value != '' && inputNroDocPaciente.value != undefined){
                //Registrar
                var dataSavePaciente = {
                    idTipoDocIde:0,
                    nroDocIde:"",
                    apellidoPaterno:"",
                    apellidoMaterno:"",
                    nombres:"",
                    idSexo:0,
                    fechaNacimiento:"",
                    lugarNacimiento:"",
                    direccion:"",
                    codUbigeo:""
                };

                dataSavePaciente.idTipoDocIde = Number(valueTwoTypeDocPaciente);
                dataSavePaciente.nroDocIde = inputNroDocPaciente.value;
                dataSavePaciente.apellidoPaterno = inputApellidoPaternoPaciente.value;
                dataSavePaciente.apellidoMaterno = inputApellidoMaternoPaciente.value;
                dataSavePaciente.nombres = inputNamesPaciente.value;
                if(valueTwoSexoPaciente != '...' && valueTwoSexoPaciente != '' && valueTwoSexoPaciente != undefined && valueTwoSexoPaciente != null){
                    dataSavePaciente.idSexo = Number(valueTwoSexoPaciente);
                }else{
                    dataSavePaciente.idSexo = 1;
                }
                dataSavePaciente.fechaNacimiento = inputFechaNacimiento.value;
                dataSavePaciente.lugarNacimiento = inputLugarNacimiento.value;
                dataSavePaciente.direccion = direccionPaciente.value;
                if(valueTwoDeparPaciente != '...' && valueTwoDeparPaciente != '' && valueTwoDeparPaciente != null && valueTwoDeparPaciente != undefined 
                && valueTwoProvinciaPaciente != '...' && valueTwoProvinciaPaciente != '' && valueTwoProvinciaPaciente != null && valueTwoProvinciaPaciente != undefined
                 && valueTwoDistritoPaciente != '...' && valueTwoDistritoPaciente != '' && valueTwoDistritoPaciente != null && valueTwoDistritoPaciente != undefined){
                    dataSavePaciente.codUbigeo = valueTwoDeparPaciente + '' + valueTwoProvinciaPaciente + '' + valueTwoDistritoPaciente;
                }else{
                    dataSavePaciente.codUbigeo = "150101";
                }

                if(valueDocAcompanante != null && valueDocAcompanante != '' && valueDocAcompanante != '...' && valueDocAcompanante != undefined){
                    if(inputNroDocAcompanante.value != null && inputNroDocAcompanante.value != '' && inputNroDocAcompanante.value != undefined){
                        if(this.pacienteEdit?.idPaciente != '' && this.pacienteEdit?.idPaciente != null && this.pacienteEdit.idPaciente != undefined){
                            idPacienteResponse = Number(await this.actualizarPaciente(dataSavePaciente, Number(this.pacienteEdit.idPaciente)));
                        }else{
                            idPacienteResponse = Number(await this.createPaciente(dataSavePaciente));
                        }

                        var dataSaveAcompanante = {
                            idPaciente:0,
                            idTipoDocIde:0,
                            nroDocIde:"",
                            apellidoPaterno:"",
                            apellidoMaterno:"",
                            nombres:"",
                            fechaNacimiento:"",
                            idParentesco:0,
                            nroTelefonoContacto:"",
                            direccion:"",
                            codUbigeo:""
                        };
        
                        dataSaveAcompanante.idTipoDocIde = Number(valueTwoDocAcompanante);
                        dataSaveAcompanante.nroDocIde = inputNroDocAcompanante.value;
                        dataSaveAcompanante.apellidoPaterno = inputApellidoPaternoAcompanante.value;
                        dataSaveAcompanante.apellidoMaterno = inputApellidoMaternoAcompanante.value;
                        dataSaveAcompanante.nombres = inputNamesAcompanante.value;
                        dataSaveAcompanante.fechaNacimiento = inputFechaNacimientoAcompanante.value;
                        
                        if(valueTwoParentescoAcompanante != '...' && valueTwoParentescoAcompanante != '' && valueTwoParentescoAcompanante != null && valueTwoParentescoAcompanante != undefined){
                            dataSaveAcompanante.idParentesco = Number(valueTwoParentescoAcompanante);
                        }
                        
                        dataSaveAcompanante.nroTelefonoContacto = inputTelefonoContactoAcompanante.value;
                        dataSaveAcompanante.direccion = inputDireccionAcompanante.value;
                        if(valueTwoDeparAcompanante != '...' && valueTwoDeparAcompanante != '' && valueTwoDeparAcompanante != null && valueTwoDeparAcompanante != undefined 
                        && valueTwoProvinciaAcompanante != '...' && valueTwoProvinciaAcompanante != '' && valueTwoProvinciaAcompanante != null && valueTwoProvinciaAcompanante != undefined
                        && valueTwoDistritoAcompanante != '...' && valueTwoDistritoAcompanante != '' && valueTwoDistritoAcompanante != null && valueTwoDistritoAcompanante != undefined){
                            dataSaveAcompanante.codUbigeo = valueTwoDeparAcompanante + '' + valueTwoProvinciaAcompanante + '' + valueTwoDistritoAcompanante;
                        }
                        console.log(dataSaveAcompanante);
                        dataSaveAcompanante.idPaciente = idPacienteResponse;

                        if(this.acompananteEdit?.idAcompanante != '' && this.acompananteEdit?.idAcompanante != null && this.acompananteEdit.idAcompanante != undefined){
                            var idAcompanante = await this.actualizarAcompanante(dataSaveAcompanante, Number(this.acompananteEdit.idAcompanante));
                        }else{
                            var idAcompanante = await this.createAcompanante(dataSaveAcompanante);
                        }
                        console.log(dataSavePaciente);
                         window.location.reload();
                    }else{
                        alert("Debe ingresar un Numero de Documento Acompanante, Es Requerido!")
                    }
                }else{
                    if(this.pacienteEdit?.idPaciente != '' && this.pacienteEdit?.idPaciente != null && this.pacienteEdit.idPaciente != undefined){
                        idPacienteResponse = Number(await this.actualizarPaciente(dataSavePaciente, Number(this.pacienteEdit.idPaciente)));
                    }else{
                        idPacienteResponse = Number(await this.createPaciente(dataSavePaciente));
                    }
                    console.log(dataSavePaciente);
                    window.location.reload();
                }
            }else{
                alert('Debe ingresar un Numero de Documento, Es Requerido!');
            }
        }else{
            alert("Debe Seleccionar un tipo de documento, Es Requerido!");
        }
    }
}