import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreatePaciente } from './create';
import { Home } from './home';
import { FindPaciente } from './find';
import { AppService } from './app.service';

const routes : Routes = [
  { path : 'create', component : CreatePaciente},
  { path : '', component : Home},
  { path : 'find', component : FindPaciente}
];

@NgModule({
  declarations: [ 
  ],
  imports: [ RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
