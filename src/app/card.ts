import {Component, Input} from '@angular/core';
//import { Destination } from './destination';

@Component({
    selector: 'card-router',
    templateUrl: './card.html',
    styleUrls: ['./card.css']
})

export class CardRouter{

    @Input() destination?: String
    @Input() description?: String
    @Input() imageAction?: String
    @Input() urlAction?: String

}